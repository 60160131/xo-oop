/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import xo_oop.Player;
import xo_oop.Table;

/**
 *
 * @author ACER
 */
public class TableUnitTest {
    
    public TableUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testSwitchPlayer() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.switchPlayer();
        assertEquals('x', table.getCurrentPlayer().getName());
    }
    
    @Test
    public void testRowWin() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(1,2);
        table.setRowCol(1,3);
        assertEquals(true, table.checkWin());
    }
    
    @Test
    public void testColWin() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(2,1);
        table.setRowCol(3,1);
        assertEquals(true, table.checkWin());
    }
    
    @Test
    public void testOblique1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(2,2);
        table.setRowCol(3,3);
        assertEquals(true, table.checkWin());
    }
    
    @Test
    public void testOblique2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,3);
        table.setRowCol(2,2);
        table.setRowCol(3,1);
        assertEquals(true, table.checkWin());
    }
    
    @Test
    public void testGetWinner() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(1,2);
        table.setRowCol(1,3);
        table.checkWin();
        assertEquals(table.getCurrentPlayer(), table.getWinner());
    }
    
    @Test
    public void testDraw() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        table.setRowCol(1, 2);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());
    }


}
