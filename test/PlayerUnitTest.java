/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import xo_oop.Player;
import xo_oop.Table;

/**
 *
 * @author ACER
 */
public class PlayerUnitTest {
    
    public PlayerUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testGetName() {
        Player x = new Player('x');
        assertEquals('x', x.getName());
    }
    
    @Test
    public void testWin() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(2,1);
        table.setRowCol(3,1);
        table.checkWin();
        assertEquals(1, table.getCurrentPlayer().getWin());
    }
    
    @Test
    public void testLose() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,1);
        table.setRowCol(2,1);
        table.setRowCol(3,1);
        table.checkWin();
        table.switchPlayer();
        assertEquals(1, table.getCurrentPlayer().getLose());
    }
    
    @Test
    public void testDraw() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1, 1);
        table.switchPlayer();
        table.setRowCol(2, 1);
        table.switchPlayer();
        table.setRowCol(3, 1);
        table.switchPlayer();
        table.setRowCol(2, 2);
        table.switchPlayer();
        table.setRowCol(2, 3);
        table.switchPlayer();
        table.setRowCol(1, 2);
        table.switchPlayer();
        table.setRowCol(3, 2);
        table.switchPlayer();
        table.setRowCol(3, 3);
        table.switchPlayer();
        table.setRowCol(1, 3);
        table.switchPlayer();
        table.checkWin();
        assertEquals(1, o.getDraw());
    }
}
