/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xo_oop;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Game {
    
    private Table table = null;
    private int row;
    private int col;
    private Player o = null;
    private Player x = null;
    public static Scanner sc = new Scanner(System.in);


    public Game() {
        this.o = new Player('o');
        this.x = new Player('x');
    }
    
    public void showHeader() {
        System.out.println(" OX Game ");
    }
    
    public static char[][] data = {{'-', '-', '-'},
                                   {'-', '-', '-'},
                                   {'-', '-', '-'}};
    
    public void showTable() {
        char[][] data = this.table.getData();
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                System.out.print(" "+data[row][col]);
            }
            System.out.println();
        }

    }
    
    public void run(){
        while(true) {
           this.runOnce();
           if(!askContinue()){
             return;  
           }
        }
    }

    public void runOnce() {
        this.showHeader();
        this.newGame();
        while(true){
            this.showTable();
            this.showTurn();
            this.inputRowCol(); 
            if(table.checkWin()){
                if(table.getWinner()!= null){
                    this.showTable();
                    System.out.println(table.getWinner().getName()+" Win!");
                }else{
                    this.showTable();
                    System.out.println("Draw!");
                }
                this.showStat();
                return;
            }
            table.switchPlayer();
        }
        
    }
    
    public int getRandomNumber(int min, int max) {
        return (int)((Math.random()*(max-min)) + min);
    }
    
    public void newGame(){
        if (getRandomNumber(1,100)%2 == 0) {
            this.table = new Table(o,x);
        }else {
            this.table = new Table(x,o);
        }
    }
    
    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName()+ " Turn");
    }
    
    public void input() {
        while(true){
            try {
                System.out.print("input Row Col : ");
                this.row = sc.nextInt();
                this.col = sc.nextInt();
                return;
            }catch(InputMismatchException iE) {
                sc.next();
                System.out.println("Error : Please input number 1-3");
                
            } 
        }
    }
    public void inputRowCol(){
        while(true) {
            this.input();
            try{
                if(table.setRowCol(row, col)){
                    return;
                } 
            }catch(ArrayIndexOutOfBoundsException e){
                System.out.println("Error : Please input number 1-3");
            }
        }   
    }
    
    public boolean askContinue(){
        while(true){
            System.out.print("Continue Yes/No ?: ");
            String ans = sc.next();
            if(ans.equals("No")){
                return false;
            }else if(ans.equals("Yes")){
                return true;
            }
        }
    }
    
    public void showStat(){
        System.out.println(o.getName()+" (win, lose, draw): "+ o.getWin()+", "+o.getLose()+", "+ o.getDraw());
        System.out.println(x.getName()+" (win, lose, draw): "+ x.getWin()+", "+x.getLose()+", "+ x.getDraw());
    }
    
}